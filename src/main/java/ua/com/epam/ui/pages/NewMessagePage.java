package ua.com.epam.ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.decorator.elements.Button;
import ua.com.epam.decorator.elements.Input;
import ua.com.epam.decorator.elements.Label;
import ua.com.epam.utils.ReportAttachments;
import ua.com.epam.utils.WaitUtils;

import static ua.com.epam.constants.Constants.RECIPIENT_XPATH;


public class NewMessagePage extends AbstractPage {

    @FindBy(css = "div.wO.nr.l1 textarea")
    private Input recipientsField;

    @FindBy(name = "subjectbox")
    private Input subjectField;

    @FindBy(xpath = "//*[@role='textbox']")
    private Input messageField;

    @FindBy(xpath = "//input[@name='subject']")
    private Label subject;

    @FindBy(xpath = "//*[@aria-label='Save & close']")
    private Button closeButton;

    @FindBy(xpath = "//*[contains(@data-tooltip,'Enter')]")
    private Button sendMessageButton;


    public void setRecipientsField(String login) {
        ReportAttachments.logInfo("Fill in recipient field");
        recipientsField.waitForFieldReadyToInput();
        recipientsField.sendKeys(login, Keys.ENTER);
    }

    public void setSubjectField(String subject) {
        ReportAttachments.logInfo("Fill in subject field");
        subjectField.waitForFieldReadyToInput();
        subjectField.sendKeys(subject);
    }

    public void setMessageField(String message) {
        ReportAttachments.logInfo("Fill in message field");
        messageField.waitForFieldReadyToInput();
        messageField.sendKeys(message);
    }

    public void clickCloseButton() {
        ReportAttachments.logInfo("Click close button");
        closeButton.saveClick();
    }

    public void clickSendMessageButton() {
        ReportAttachments.logInfo("Click send message button");
        WaitUtils.waitForElementPresence(By.xpath(RECIPIENT_XPATH));
        sendMessageButton.saveClick();
    }
}
