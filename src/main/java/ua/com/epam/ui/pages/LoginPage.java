package ua.com.epam.ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.decorator.elements.Input;
import ua.com.epam.utils.ReportAttachments;

public class LoginPage extends AbstractPage {
    @FindBy(id = "identifierId")
    private Input inputLoginField;

    @FindBy(xpath = "//input[@type='password']")
    private Input inputPasswordField;

    public void setLoginField(String login) {
        ReportAttachments.logInfo("Fill in login field");
        inputLoginField.waitForFieldReadyToInput();
        inputLoginField.sendKeys(login, Keys.ENTER);
    }

    public void setPasswordField(String password) {
        ReportAttachments.logInfo("Fill in password field");
        inputPasswordField.waitForFieldReadyToInput();
        inputPasswordField.sendKeys(password, Keys.ENTER);
    }
}
