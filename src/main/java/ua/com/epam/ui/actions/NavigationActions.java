package ua.com.epam.ui.actions;

import io.qameta.allure.Step;
import ua.com.epam.configurations.PropertiesManager;
import ua.com.epam.factory.DriverProvider;

public class NavigationActions {
    @Step("Navigate to login page")
    public void navigateToLoginPage() {
        DriverProvider.getDriver().get(PropertiesManager.getBaseUrl());
    }
}
