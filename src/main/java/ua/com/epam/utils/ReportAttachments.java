package ua.com.epam.utils;


import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import ua.com.epam.factory.DriverProvider;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReportAttachments {
    private static final Logger logger = LogManager.getLogger(ReportAttachments.class);

    private ReportAttachments() {
    }

    @Attachment(value = "logs", type = "text", fileExtension = ".log")
    public static byte[] addFileToAllure(String path) throws IOException {
        File file = new File(path);
        return Files.readAllBytes(Paths.get(file.getPath()));
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] addScreenToAllure() {
        return ((TakesScreenshot) DriverProvider.getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    @Step("{0}")
    public static void logInfo(String message) {
        logger.info(message);
    }

    @Step("{0}")
    public static void logDebug(String message, By by) {
        logger.debug(message);
    }

    @Step("{0}")
    public static void logDebug(String message) {
        logger.debug(message);
    }
}