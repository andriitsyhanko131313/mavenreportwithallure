package ua.com.epam.configurations;

public class PropertiesManager {
    private PropertiesManager() {
    }

    public static String getBaseUrl() {
        return PropertiesReader.readProperties("base_url");
    }

    public static String getChromeDriver() {
        return PropertiesReader.readProperties("chrome_driver");
    }

    public static String getChromeDriverPath() {
        return PropertiesReader.readProperties("chrome_driver_path");
    }

    public static Integer getImplicitTime() {
        return Integer.parseInt(PropertiesReader.readProperties("implicit_time"));
    }

    public static String getUsersFilePath() {
        return PropertiesReader.readProperties("users_file_path");
    }

    public static Integer getExplicitTime() {
        return Integer.parseInt(PropertiesReader.readProperties("explicit_time"));
    }

    public static String getLetterFilePath() {
        return PropertiesReader.readProperties("letter_file_path");
    }

    public static String getLogsFilePath() {
        return PropertiesReader.readProperties("logs_file_path");
    }
}

