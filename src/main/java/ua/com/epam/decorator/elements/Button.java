package ua.com.epam.decorator.elements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ua.com.epam.decorator.PageElement;
import ua.com.epam.utils.ReportAttachments;
import ua.com.epam.utils.WaitUtils;

public class Button extends PageElement {
    private static final Logger logger = LogManager.getLogger(Button.class);

    public Button(WebElement webElement, By by) {
        super(webElement, by);
    }

    public void saveClick() {
        ReportAttachments.logDebug("Wain element with locator to be clickable with locator", getLocator());
        WaitUtils.elementToBeClickable(this).click();
    }

}
