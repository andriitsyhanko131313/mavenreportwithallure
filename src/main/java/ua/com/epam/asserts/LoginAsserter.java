package ua.com.epam.asserts;

import io.qameta.allure.Step;
import org.testng.Assert;
import ua.com.epam.ui.pages.HomePage;

public class LoginAsserter {
    private final HomePage homePage;

    public LoginAsserter() {
        homePage = new HomePage();
    }

    @Step("Verify that login is: [{userLogin}]")
    public void assertSuccessfulLogIn(String userLogin) {
        String actual = homePage.getAccountInformation();
        Assert.assertTrue(actual.contains(userLogin),
                String.format("Expected user login  [%s], but found [%s]", userLogin, actual));
    }

}

