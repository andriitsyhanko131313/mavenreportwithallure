package ua.com.epam;

import org.testng.annotations.Test;
import ua.com.epam.asserts.LoginAsserter;
import ua.com.epam.asserts.MessageAsserter;
import ua.com.epam.ui.actions.MessageActions;
import ua.com.epam.ui.actions.LoginActions;
import ua.com.epam.ui.actions.NavigationActions;
import ua.com.epam.utils.FileManager;
import ua.com.epam.utils.Letter;
import ua.com.epam.utils.User;

import static ua.com.epam.constants.Constants.*;

public class SendMessageFromDraftsTest extends BaseTest {


    @Test(description = "Verify that message is saved as draft and send message")
    public void sendMessageFromDraftsTestCase() {
        NavigationActions navigationAction = new NavigationActions();
        LoginActions loginActions = new LoginActions();
        MessageActions messageAction = new MessageActions();
        LoginAsserter loginAsserter = new LoginAsserter();
        MessageAsserter messageAsserter = new MessageAsserter();
        Letter letter = FileManager.getLetter();
        User user = FileManager.getUser();

        navigationAction.navigateToLoginPage();
        loginActions.logInToAccount(user.getLogin(), user.getPassword());
        loginAsserter.assertSuccessfulLogIn(user.getLogin());

        messageAction.fillInNewMessage(letter);
        messageAction.closeMessageForm();
        messageAction.openDrafts();
        messageAsserter.assertMessageAddedToDrafts(letter.getSubject());

        messageAction.openMessage(letter.getSubject());
        messageAction.sendMessage();
        messageAsserter.assertMessageSent(SUCCESSFULLY_SENT_MESSAGE);
    }
}
